import {Component, OnInit} from '@angular/core'

import {AuthenticationService} from './authentication.service'
import {Router} from '@angular/router'


@Component({
    selector: 'app-auth-signout',
    templateUrl : 'js/app/auth/signout.component.html' 
})
export class SignoutComponent implements OnInit
{

    
    constructor(private _svc : AuthenticationService,
    private _router : Router){}


    ngOnInit()
    { 
        if (!this._svc.isSignedIn())
        {  
            this._router.navigateByUrl('/');
        }
    }

    onLogout()
    {
        this._svc.signOutToken();
        this._router.navigateByUrl('/');
    }
}