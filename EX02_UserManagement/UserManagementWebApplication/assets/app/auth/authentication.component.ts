

import { Component } from '@angular/core';    
import { Routes, ROUTER_DIRECTIVES } from '@angular/router';  
import { SignupComponent } from './signup.component'
import { SignoutComponent } from './signout.component'
import { SigninComponent } from './signin.component'

import {AuthenticationService} from './authentication.service'

@Component({
    selector: 'my-app-auth',
    templateUrl : 'js/app/auth/authentication.component.html',
    directives : [ROUTER_DIRECTIVES],
    styles : [ `
        .router-link-active
        {
            color: #555;
            cursor: default;
            background-color: #fff;
            border: 1px solid #ddd;
            border-bottom-color: transparent;
        }
    ` ]

})  
@Routes([
    {path:"/signin", component: SigninComponent},
    {path:"/signup", component: SignupComponent},
    {path:"/signout", component: SignoutComponent}
])
export class AuthenticationComponent
{

    constructor(private _svc : AuthenticationService){}

    public isSignedIn() : boolean
    {
        return this._svc.isSignedIn();
    }

}