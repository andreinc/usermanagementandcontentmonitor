import {Component, OnInit} from '@angular/core'
import {FormBuilder, ControlGroup, Validators, Control} from '@angular/common'
import {AuthenticationService} from './authentication.service'
import {Router} from '@angular/router'
import {ErrorService} from '../errors/error.service';

import {User} from './user'

@Component({
    selector: 'app-auth-signin',
    templateUrl : 'js/app/auth/signin.component.html' 
})
export class SigninComponent
{
  myForm : ControlGroup;

    constructor(private _fb : FormBuilder,
    private _errSvc : ErrorService,
    private _svc: AuthenticationService,
    private _router : Router){}


    onSubmit()
    {
        console.log(this.myForm.value);

        
        const usr = new User(
            this.myForm.value.email,
            this.myForm.value.password,
            null,
            null
        );

        this._svc.signin(usr).subscribe(
            data => {
             //   console.log(data);
 
                this._router.navigateByUrl('/')
            },
            error => this._errSvc.handleError(error)
        );
    }
    ngOnInit()
    {
            this.myForm = this._fb.group(
                { 
                    email : ['', 
                    
                    Validators.compose([

                        Validators.required,
                        this.isEmail 
                    ]) 
                    ],
                    password : ['', Validators.required]
                }
            );
    }

   private isEmail(control: Control): {[s: string]: boolean} {
        if (!control.value.match("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")) {
            return {invalidMail: true};
        }
    }
}