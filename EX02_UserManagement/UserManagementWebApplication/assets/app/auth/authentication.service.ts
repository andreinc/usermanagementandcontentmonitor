import {Injectable, EventEmitter, Output} from '@angular/core'

import {Http, Headers} from '@angular/http';

import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';


import {User} from './user'


@Injectable() 
export class AuthenticationService
{

    constructor(private _http : Http){}

    private TOKEN_NAME : string = 'token';
    private UID_NAME : string = 'userID';

    private hdrs = new Headers({'Content-Type':'application/json'});
    private API_URL = 'http://localhost:3000/user';
 

    isSignedIn() : boolean
    {
        let tkn = localStorage.getItem(this.TOKEN_NAME);
 
        return tkn !== null;
    }

    
    public get tokenUserId() : string {
        return localStorage.getItem(this.UID_NAME);
    }
    

    signInToken(userid, token) : void
    { 
        localStorage.setItem(this.UID_NAME, userid);
        localStorage.setItem(this.TOKEN_NAME, token);
    }

    signOutToken()
    {
        localStorage.removeItem(this.TOKEN_NAME);
        localStorage.removeItem(this.UID_NAME);
    }

    getTokenQueryParam() : any
    {
        var q = '';

        if (this.isSignedIn())
        {
            q = '?token=' + localStorage.getItem(this.TOKEN_NAME);
        }
    }


    signup(user : User)
    {
        const body = JSON.stringify(user); 
  
        return this._http.post(this.API_URL, body, {headers: this.hdrs})
        .map(response => 
        { 
                const data = 
                    response.json().obj;

                    let usr = new User(
                        data.email, 
                        null,
                        data.firstName,
                        data.lastName

                    );

                    return usr;
        }        )
        .catch(error => Observable.throw(error.json()));
    }

    signin(user : User)
    {
        const body = JSON.stringify(user); 
  
        return this._http.post(this.API_URL + "/signin", body, {headers: this.hdrs})
        .map(response => 
        { 
                //the token
                const data = 
                    response.json();

                    this.signInToken(data.userid, data.obj);
 
                    return data.obj;
        }        )
        .catch(error => Observable.throw(error.json()));
    }

}