import {Component, OnInit} from '@angular/core'
import {FormBuilder, ControlGroup, Validators, Control} from '@angular/common'
import {AuthenticationService} from './authentication.service'
import {Router} from '@angular/router'
import {ErrorService} from '../errors/error.service';


import {User} from './user'


@Component({
    selector: 'app-auth-signup',
    templateUrl : 'js/app/auth/signup.component.html' 
})
export class SignupComponent implements OnInit
{

    myForm : ControlGroup;

    constructor(private _fb : FormBuilder,
    private _errSvc : ErrorService,

    private _svc: AuthenticationService,
    private _router : Router){}


    onSubmit()
    {
        console.log(this.myForm.value);

        const usr = new User(
            this.myForm.value.email,
            this.myForm.value.password,
            this.myForm.value.firstName,
            this.myForm.value.lastName 
        );

        this._svc.signup(usr).subscribe(
            data =>
            { 
                console.log(data);
               // this._router.navigateByUrl('./signin');
            } ,
            error =>  this._errSvc.handleError (error)
        );
    }
    ngOnInit()
    {
            this.myForm = this._fb.group(
                {
                    firstName: ['', Validators.required],
                    lastName: ['', Validators.required],
                    email : ['', 
                    
                    Validators.compose([

                        Validators.required,
                        this.isEmail

                    ])
                    
                    
                    
                    ],
                    password : ['', Validators.required]
                }
            );
    }

   private isEmail(control: Control): {[s: string]: boolean} {
        if (!control.value.match("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")) {
            return {invalidMail: true};
        }
    }

}