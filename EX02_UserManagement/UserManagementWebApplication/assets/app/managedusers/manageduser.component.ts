
import {Component, Input, Output, EventEmitter, Injectable } from '@angular/core';
import {ManagedUser} from './manageduser' ; 

import {AuthenticationService} from '../auth/authentication.service'

@Component({
    selector: 'app-manageduser',
    styleUrls : ['js/app/managedusers/manageduser.component.css'],
    templateUrl : 'js/app/managedusers/manageduser.component.html' 
}) 
export class ManagedUserComponent
{
 
    constructor( private _svc : AuthenticationService){}

    public bgcolor : string = "white";

    public show : boolean = true;

    @Input('cmManagedUser')
    manageduser : ManagedUser;  

    @Output("cmEditClicked")
    editClicked = new EventEmitter<ManagedUser>();

    
    @Output("cmDeleteClicked")
    deleteClicked = new EventEmitter<ManagedUser>();

    onEditClick()
    {
        this.editClicked.emit(this.manageduser);
    }
    onDeleteClick()
    {
        this.deleteClicked.emit(this.manageduser);
    }

    isSignedIn() : boolean
    {
        return this._svc.isSignedIn();
    }
     
}