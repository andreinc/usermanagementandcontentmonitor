
import { Component, Input, Output, OnInit } from '@angular/core'
import { ManagedUserComponent } from './manageduser.component'
import { ManagedUser } from './manageduser'
import { ManagedUserService } from './manageduser.service'

@Component(
    {
        selector: "app-manageduser-list",
        templateUrl :  'js/app/managedusers/manageduser-list.component.html'      ,
        directives: [ManagedUserComponent] 
        
    }
)  
export class ManagedUserListComponent implements OnInit {

    constructor(private _svc: ManagedUserService) {}

    managedusers : ManagedUser[];

    ngOnInit() {
        //console.log("ngOnInit occured");
        this.getAllManagedUsers();
    }

    getAllManagedUsers()
    {
         this._svc.getManagedUsers()
            .subscribe(
                musrs =>  
                {
                    this.managedusers = musrs;
                    this._svc.managedusers = this.managedusers; 
                }
            );

    }

 	itemEdit(itm : ManagedUser) 
    {
        this._svc.editManagedUser(itm);
    }

    
 	itemDelete(itm : ManagedUser) 
    {
        this._svc.deleteManagedUser(itm)
                .subscribe(
                    data => console.log(data),
                    error => console.error(error)
                );
    }
}