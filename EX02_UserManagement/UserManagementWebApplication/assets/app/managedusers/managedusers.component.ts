
import { Component } from '@angular/core';   
import { ManagedUserFormComponent } from './manageduser-form.component';
import { ManagedUserListComponent } from './manageduser-list.component';


@Component({
    selector: 'my-app-content',
    templateUrl : 'js/app/managedusers/managedusers.component.html' ,
    directives: [ManagedUserFormComponent, ManagedUserListComponent] 
})  
export class ManagedUsersComponent
{
    HandleNewManagedUser(newManagedUser)
    {  
        console.log(newManagedUser);
    } 
}

