import {Injectable, EventEmitter, Output} from '@angular/core'

import {Http, Headers} from '@angular/http';

import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';


import {ManagedUser} from './manageduser'


@Injectable() 
export class ManagedUserService
{

    constructor(private _http : Http){}

    private TOKEN_NAME : string = 'token';
    private UID_NAME : string = 'userID';
    private hdrs = new Headers({'Content-Type':'application/json'});
    private API_URL = 'http://localhost:3000/manageduser/';
 
    public managedusers: ManagedUser[] = [];
 
    @Output("cmEditClicked")
    itemEditClicked : EventEmitter<ManagedUser> = new EventEmitter<ManagedUser>();


    getTokenQueryParam() : any
    {
        var q = '';
        var t = localStorage.getItem(this.TOKEN_NAME);
        if (t)
        {
            q = '?token=' + t;
        }

        return q;
    }

 
    addManagedUser(manageduser : ManagedUser)
    {
        const body = JSON.stringify(manageduser);
  
        return this._http.post(this.API_URL +  this.getTokenQueryParam() , body, {headers: this.hdrs})
        .map(response => 
        { 
                const data = 
                    response.json().obj;

                    //TODO: Create the object
                    let mu = new ManagedUser( 
                        	//properties
                            data.email,
                            data.firstName ,
                            data.lastName ,
                            data.addressLine1 ,
                            data.addressLine2 ,
                            data.addressCity,
                            data.addressState ,
                            data.addressZip ,
                            data.isDeleted , 
                            data._id
                    );

                    return mu;
        }        )
        .catch(error => Observable.throw(error.json()));
    }

    deleteManagedUser(manageduser: ManagedUser)
    {

        const body = JSON.stringify(manageduser); 
  
        this.managedusers.splice(this.managedusers.indexOf(manageduser), 1);
 
        return this._http.delete(this.API_URL + manageduser.userId +  this.getTokenQueryParam())
            .map(response =>  response.json() )
            .catch(error => Observable.throw(error.json()));
            
             

    }

    editManagedUser(manageduser: ManagedUser)
    { 
        this.itemEditClicked.emit(manageduser); 
    }

    updateManagedUser(manageduser: ManagedUser)
    {
        const body = JSON.stringify(manageduser); 
  
        return this._http.patch(this.API_URL + manageduser.userId +  this.getTokenQueryParam(), 
        body, {headers: this.hdrs})
            .map(response =>  response.json() )
            .catch(error => Observable.throw(error.json()));
    }

    getManagedUsers()
    {  
      	return this._http.get(this.API_URL )
        .map(response => 
        { 
                const data = 
                    response.json().obj;

                    //data must be cast as array of type Message 

                    let msgSet: any[] = [];
                    for (var i = 0; i < data.length; i++) {
  
                        let msg = new ManagedUser(
                            //TODO: Read Properties
                            data[i].email,
                            data[i].firstName ,
                            data[i].lastName ,
                            data[i].addressLine1 ,
                            data[i].addressLine2 ,
                            data[i].addressCity,
                            data[i].addressState ,
                            data[i].addressZip ,
                            data[i].isDeleted ,
                            data[i]._id
                        ) ;
                        msgSet.push(msg);
                    }

                    return msgSet;
        }        )
        .catch(error => Observable.throw(error.json()));
    }

}