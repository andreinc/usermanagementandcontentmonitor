
import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core'
import {ManagedUser} from './manageduser' 
import {ManagedUserService} from './manageduser.service'; 
import {ErrorService} from '../errors/error.service'

@Component(
    {
        selector: "app-manageduser-form",
        templateUrl :  'js/app/managedusers/manageduser-form.component.html' 
    }
)
export class ManagedUserFormComponent implements OnInit
{

    manageduser : ManagedUser = null;

    constructor( 
    private _errSvc : ErrorService,
    private _svc : ManagedUserService){
 
    }

    ngOnInit()
    {
        this._svc.itemEditClicked
                    .subscribe(
                        (item) => 
                            {this.manageduser = item;}
                    );
    }

    @Output("cmNewManagedUserCreated")
    newManagedUserCreated = new EventEmitter<ManagedUser>();

    onCancel()
    {
        this.manageduser = null;
    }

    onSubmitContent(form: any)
    {

        if (this.manageduser)
        {
            //edit mode
            this.manageduser.email = form.email;
            this.manageduser.firstName = form.firstName ;
            this.manageduser.lastName = form.lastName ;
            this.manageduser.addressLine1 = form.addressLine1 ;
            this.manageduser.addressLine2 = form.addressLine2 ;
            this.manageduser.addressCity = form.addressCity;
            this.manageduser.addressState = form.addressState;
            this.manageduser.addressZip = form.addressZip;
            
            this._svc.updateManagedUser(this.manageduser)
                    .subscribe(
                        data => {   console.log(data); 
                                    this.manageduser = null;   
                        },
                        error => {  this._errSvc.handleError(error) }
                    ); 
        }
        else
        {
            //get user token for user info?

            const msg : ManagedUser =  new ManagedUser( 
                
                    form.email,
                    form.firstName ,
                    form.lastName ,
                    form.addressLine1 ,
                    form.addressLine2 ,
                    form.addressCity,
                    form.addressState,
                    form.addressZip
              ); 

            this._svc.addManagedUser(msg)
                    .subscribe(
                        data =>{ 
                                this.newManagedUserCreated.emit(msg);
                                this._svc.managedusers.push(data);
                        },
                        error => 
                        {  
                            this._errSvc.handleError(error)
                        }
                    );
 
 
        } 
         
            
        
    }
}