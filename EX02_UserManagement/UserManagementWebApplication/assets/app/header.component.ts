import { Component } from "@angular/core";

import { ROUTER_DIRECTIVES } from "@angular/router";
@Component({
    selector: 'app-header',
    templateUrl : 'js/app/header.component.html',
    directives: [ROUTER_DIRECTIVES],
    styleUrls: ['js/app/header.component.css'],
})
export class HeaderComponent {
    
}