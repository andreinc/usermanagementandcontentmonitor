
import {EventEmitter} from "@angular/core";
import {Error} from './error'

export class ErrorService
{


    errorOccured = new EventEmitter<Error>();

    handleError(err: any)
    {
        debugger;
        const errorData = new Error(err.title, err.error.message);

        this.errorOccured.emit(errorData);
    }

}