///<reference path="../../typings.d.ts"/>
import { bootstrap } from '@angular/platform-browser-dynamic';
import { ROUTER_PROVIDERS } from "@angular/router";
import { LocationStrategy, HashLocationStrategy } from "@angular/common";
import { provide } from "@angular/core";

import {HTTP_PROVIDERS} from '@angular/http';

import { AppComponent } from "./app.component";
import { ManagedUserService } from "./managedusers/manageduser.service";

import {AuthenticationService} from './auth/authentication.service'

import {ErrorService} from './errors/error.service';

bootstrap(AppComponent, 
            [ManagedUserService, AuthenticationService, ErrorService,
            ROUTER_PROVIDERS, 
            provide(LocationStrategy, {useClass: HashLocationStrategy}),
            HTTP_PROVIDERS

            ]
);