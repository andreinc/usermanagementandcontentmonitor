import {Component} from '@angular/core';  
import {Routes, ROUTER_DIRECTIVES} from '@angular/router';  
import {ManagedUsersComponent} from './managedusers/managedusers.component';
import {AuthenticationComponent} from './auth/authentication.component';
import {HeaderComponent} from './header.component'; 
import {ErrorComponent} from './errors/error.component';

@Component({
    selector: 'my-app',
    templateUrl : 'js/app/app.component.html',
    styleUrls: ['js/app/app.component.css'],
    directives: [
        ErrorComponent,
        ROUTER_DIRECTIVES,
        AuthenticationComponent,
        HeaderComponent, ManagedUsersComponent] 
}) 
@Routes([
    {path: '/', component: ManagedUsersComponent},
    {path: '/auth', component: AuthenticationComponent},
])
export class AppComponent {
 

}