var express = require('express');
var router = express.Router(); 

var User = require('../models/user');
var hasher = require('password-hash');
var jwt = require('jsonwebtoken');

router.get('/', function (req, res, next) {
    
    return res.status(501).json(
                    {
                        title: 'request not implemented',
                        error: err
                    }
                )
});

 
 
router.post('/signin', function(req, res, next){
    
    User.findOne(
        {email : req.body.email}, 
        function(err, doc){
        if (err)
        { 
                return res.status(400).json(
                    {
                        title: 'an error occured',
                        error: err
                    }
                );
        }
        if (!doc)
        {

                return res.status(404).json(
                    {
                        title: 'Not Found',
                        error: {message: 'user not found'}
                    }
                );
        }
        if (!hasher.verify(req.body.password, doc.password))
        {
                return res.status(401).json(
                    {
                        title: 'Invalid Password',
                        error: {message: 'Invalid Password'}
                    }
                );
        }
        else
        {
                var token = jwt.sign({
                    user: doc
                }, 'secret', {expiresIn: 7200});

                
            res.status(200).json({
                message: 'Success',
                obj: token,
                userid: doc._id
            });
        }
    });
}
);

 
router.post('/', function(req, res, next){
 
    var usr = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hasher.generate(req.body.password)

    })

    usr.save(
        function(err, result)
        {
            if (err)
            {
                return res.status(400).json(
                    {
                        title: 'an error occured',
                        error: err
                    }
                )
            }
            res.status(200).json({
                message: 'Success',
                obj: result
            });
        }
    );

});


 

module.exports = router;
