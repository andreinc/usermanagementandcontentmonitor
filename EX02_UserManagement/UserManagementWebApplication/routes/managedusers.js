var express = require('express');
var router = express.Router(); 
var jwt = require('jsonwebtoken');

var DocModel = require('../models/manageduser');
var User = require('../models/user');


router.get('/', function (req, res, next) {
    
        DocModel.find({ isDeleted: false }) 
            .exec(
                function(err, docs)
                {
                    if (err)
                    {
                        res.status(404).json(
                            {
                                message: 'An error ocuured',
                                error: err
                            });
                    }
                    if (docs)
                    {
                        res.status(200).json(
                                    {
                                        message: 'Success',
                                        obj: docs
                                    });
                    }
                    else
                    {
                           res.status(404).json(
                            {
                                message: 'An error ocuured',
                                error: err
                            });
                    }
                }
            );
});


router.use('/', function (req, res, next){

    jwt.verify(req.query.token, 'secret', function(err, resp){

                    if (err)
                    {
                        res.status(401).json(
                            {
                                title: 'Not Authorized',
                                error: {message: 'jwt token is not valid'}
                            });
                    }

                   next();

    });

});

 
router.post('/', function(req, res, next){

    var tkn = jwt.decode(req.query.token);
    var userObj = tkn.user;
    var usr = User.findById(userObj, function(err, doc){
                if (err)
                {
                    return res.status(404).json(
                        {
                            title: 'SystemUser not found',
                            error: err 
                        }
                    );
                }

            var aDoc = new DocModel({
                email : req.body.email,
                firstName : req.body.firstName,
                lastName : req.body.lastName, 
                addressLine1 : req.body.addressLine1,
                addressLine2 : req.body.addressLine2,
                addressCity : req.body.addressCity,
                addressState : req.body.addressState, 
                addressZip : req.body.addressZip, 
                isDeleted : false,
                isDirty : true
                
            });



            aDoc.save(
                function(err, result)
                {
                    if (err)
                    {
                        return res.status(500).json(
                            {
                                title: 'a POST error occured',
                                error: err 
                            }
                        );
                    } 

                    res.status(200).json(
                        {
                        message: 'Success',
                        obj: result
                    });
                }
            );

    });
 


});



router.patch('/:id', function(req, res, next){

    
    var tkn = jwt.decode(req.query.token);
    var tknUser = tkn.user;

 
    DocModel.findById(req.params.id, function(err, doc){

      if (err)
      {
        return res.status(500).json(
            {
                title: 'a PATCH error occured',
                error: err 
            }
        );
      }
      if(!doc)
      {
        return res.status(404).json(
                    {
                        title: 'No message found',
                        error: {message: 'Message not found'}
                    });
      }
      //found
 
        //change content 
        doc.email = req.body.email;
        doc.firstName = req.body.firstName;
        doc.lastName = req.body.lastName;
        doc.addressLine1 = req.body.addressLine1;
        doc.addressLine2 = req.body.addressLine2;
        doc.addressCity = req.body.addressCity;
        doc.addressState = req.body.addressState; 
        doc.addressZip = req.body.addressZip;  
        doc.isDirty = true;


        doc.save( 
            function(err, result)
                {
                    if (err)
                    {
                        return res.status(500).json(
                            {
                                title: 'a PATCH/SAVE error occured',
                                error: err 
                            }
                        );
                    }
                    res.status(200).json(
                        {
                        message: 'Success',
                        obj: result
                    });
                } );
  });

});


router.delete('/:id', function(req, res, next){
 
    var tkn = jwt.decode(req.query.token);
    var tknUser = tkn.user;

    DocModel.findById(req.params.id, function(err, doc){

      if (err)
      {
        return res.status(500).json(
            {
                title: 'a DELETE error occured',
                error: err 
            }
        );
      }
      if(!doc)
      {
        return res.status(404).json(
                    {
                        title: 'No message found',
                        error: {message: 'Message not found'}
                    });
      }
        //found
 
        //Soft Delete
        doc.isDirty = true;
        doc.isDeleted = true;
 
        doc.save ( 
            function(err, result)
                {
                    if (err)
                    {
                        return res.status(500).json(
                            {
                                title: 'a DELETE error occured',
                                error: err 
                            }
                        );
                    }
                    res.status(200).json(
                        {
                        message: 'Success',
                        obj: result
                    });
                } );
  });

});

 

module.exports = router;
