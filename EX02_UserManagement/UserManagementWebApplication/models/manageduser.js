

var mongoose = require('mongoose');
var mongooseUniqueValidator = require('mongoose-unique-validator');
var Schema = mongoose.Schema;
 

var schema = new Schema(
    {
        firstName : { type: String, required: true},
        lastName : { type: String, required: true}, 
        email : { type: String, required: true, unique: true},
        addressLine1 : { type: String, required: true}, 
        addressLine2 : { type: String, required: false}, 
        addressCity : { type: String, required: true}, 
        addressState : { type: String, required: true}, 
        addressZip : { type: String, required: true}, 
        isDeleted : { type: Boolean, required: false, default: true},
        isDirty : { type: Boolean, required: false, default: true},
        contractVersion : { type: String, required: false, default: '1.0'},
    }
);

//callback to post-remove event for Message/Schema
schema.post('remove', function(doc){

        //call back following remove of document 

});


module.exports = mongoose.model('Manageduser', schema);
    