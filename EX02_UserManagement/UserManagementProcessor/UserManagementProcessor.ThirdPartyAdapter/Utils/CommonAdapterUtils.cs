﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementProcessor.Contracts.Assets;
using UserManagementProcessor.ThirdPartyAdapter.SpecContracts;

namespace UserManagementProcessor.ThirdPartyAdapter.Utils
{
    public static class CommonAdapterUtils
    {
        public enum SupportedThirdPartyMessageVersion
        {
            pre0_9 = 0,
            release1_0 = 1,
            release2_0 = 2
        }


        /// <summary>
        /// Converts and serializes to a supported spec.
        /// </summary>
        /// <param name="profileToConvert">The profile to convert.</param>
        /// <param name="targetSpec">The target spec.</param>
        /// <returns></returns>
        public static string ConvertSerializeToSupportedSpec(this UserVO profileToConvert, SupportedThirdPartyMessageVersion targetSpec)
        {
            var serialized = string.Empty;

            if (profileToConvert != null && !string.IsNullOrEmpty(profileToConvert.ContractVersion))
            {
                switch (profileToConvert.ContractVersion.ToUpper())
                {
                    case "0.0":

                        #region Build model from spec 0.9
                        var targetObj00 = new ManagedUser_PreRelease090()
                        {
                            FullName = string.Format("{0}{1}", profileToConvert.FirstName, profileToConvert.LastName),
                            FullAddress = string.Format("{0}{1}{2}{3}{4}", profileToConvert.AddressLine1, profileToConvert.AddressLine2, profileToConvert.AddressCity, profileToConvert.AddressState, profileToConvert.AddressZip)
                        };
                        serialized = JsonConvert.SerializeObject(targetObj00);
                        #endregion
                         
                        break;


                    case "1.0":

                        #region Build model from spec 1.0
                        var targetObj10 = new ManagedUser_Release100()
                        {
                            FirstName = profileToConvert.FirstName,
                            LastName = profileToConvert.LastName,
                            FullAddress = string.Format("{0}{1}{2}{3}{4}", profileToConvert.AddressLine1, profileToConvert.AddressLine2, profileToConvert.AddressCity, profileToConvert.AddressState, profileToConvert.AddressZip)
                        };
                        serialized = JsonConvert.SerializeObject(targetObj10);
                        #endregion

                        break;

                    default:
                        break;
                }
            }

            return serialized;
        }
    }
}
