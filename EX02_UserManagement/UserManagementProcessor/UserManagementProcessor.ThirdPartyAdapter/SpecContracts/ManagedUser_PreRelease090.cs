﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagementProcessor.ThirdPartyAdapter.SpecContracts
{
    public class ManagedUser_PreRelease090
    {
        public string FullName { get; set; }
        public string FullAddress { get; set; }
    }
}
