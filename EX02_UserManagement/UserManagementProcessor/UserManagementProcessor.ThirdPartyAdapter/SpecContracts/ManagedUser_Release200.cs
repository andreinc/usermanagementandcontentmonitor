﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagementProcessor.ThirdPartyAdapter.SpecContracts
{
    public class ManagedUser_Release200
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullAddress { get; set; }


    }
}
