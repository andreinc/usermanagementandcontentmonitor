﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MockThirdPartyComponent
{
    public class ExternalUserClient : IDisposable
    {
        public ExternalUserClient()
        {

        }

        public const string SupportedSpecVersion = "release1_0";

        public int Save(string globalIdentifier, string contractSerialized)
        {
            //Mock Save

            return 1;
        }

        public int Delete(string globalIdentifier)
        {
            //Mock Delete

            return 1;
        }

        public void Dispose()
        {
           //close / release res
        }
    }
}
