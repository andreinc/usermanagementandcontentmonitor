﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserManagementProcessor.Contracts.Assets
{
    public class UserVO
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        [BsonElement("firstName")]
        public string FirstName { get; set; }
        [BsonElement("lastName")]
        public string LastName { get; set; }
        [BsonElement("email")]
        public string Email { get; set; }
        [BsonElement("addressLine1")]
        public string AddressLine1 { get; set; }
        [BsonElement("addressLine2")]
        public string AddressLine2 { get; set; }
        [BsonElement("addressCity")]
        public string AddressCity { get; set; }
        [BsonElement("addressState")]
        public string AddressState { get; set; }
        [BsonElement("addressZip")]
        public string AddressZip { get; set; }
        [BsonElement("isDeleted")]
        public bool IsDeleted { get; set; }
        [BsonElement("isDirty")]
        public bool IsDirty { get; set; }
        [BsonElement("contractVersion")]
        public string ContractVersion { get; set; }

        public int __v { get; set; }
    }
}
