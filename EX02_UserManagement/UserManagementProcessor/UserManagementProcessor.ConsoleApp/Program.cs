﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementProcessor.Business.Assets;

namespace UserManagementProcessor.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        { 
            //Single call to call for processing data 

            //TODO: Create a Windows Service or process to perform this task based on an event (scheduled time, system/data event) 


            Console.WriteLine("Processing sync ...");
            #region Processing/Sync of Data

            var ctrl = new ContentProcessor();

            var results = ctrl.Process();

            #endregion

            #region Log Info/interaction to console
            if (results.Count == 0)
            {
                Console.WriteLine("No Changes detected.");
            }
            else {
                foreach (var item in results)
                {
                    Console.WriteLine(item);
                }
            }
             
            Console.WriteLine("Processing completed. Press a key to exit.");
            Console.ReadKey();
            #endregion

        }
    }
}
