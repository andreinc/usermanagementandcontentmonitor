﻿using MockThirdPartyComponent;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UserManagementProcessor.Business.Properties;
using UserManagementProcessor.Contracts.Assets;
using UserManagementProcessor.ThirdPartyAdapter.Utils;

namespace UserManagementProcessor.Business.Assets
{
    public class ContentProcessor
    {

        public ContentProcessor()
        {
            //TODO: with IoC define interface and inject Repository as needed
        }

        public List<Instruction> Process()
        {
            var summaryOfActions = new List<Instruction>();
            var allStorageUsers = new List<UserVO>();

            using (var client = new ExternalUserClient())
            {

                //TODO: Move into a Repository class
                #region Repository Init - and call to Get from storage

                var dbclient = new MongoClient(Settings.Default.DBHOST);
                var db = dbclient.GetDatabase(Settings.Default.DBNAME);
                var collection = db.GetCollection<UserVO>(Settings.Default.USERSCOLLECTIONNAME);



                //fetch qualifying data : where isDirty is true
                var filter = Builders<UserVO>.Filter.Eq("isDirty", true);
                allStorageUsers = collection.Find(filter).ToList();

                #endregion

                 

                //holds documents converted towards target spec
                var serializedInstructionSet = new Dictionary<string, Instruction>();

                foreach (var item in allStorageUsers)
                {
                    //extension with a reference to the third party contract - return a serialized string for wire 
                    var ser = item.ConvertSerializeToSupportedSpec(CommonAdapterUtils.SupportedThirdPartyMessageVersion.release1_0);

                    //default behaviour : SAVE
                    // dirty:1, deleted:0 -> SAVE
                    var action = ActionName.Save;


                    // dirty:1, deleted:1 -> DELETE
                    if (item.IsDeleted && item.IsDirty)
                    {
                        action = ActionName.Delete;
                    }

                    //add instruction
                    serializedInstructionSet.Add(item._id, new Instruction { Identifier = item._id , ActionName = action, Content = ser });
                }

                var actionManifest = new Dictionary<string, int>();
                //call third party action ()
                foreach (var inst in serializedInstructionSet)
                {
                    switch (inst.Value.ActionName)
                    {
                        case ActionName.Delete:
                            //DELETE
                            var deleteResult  = client.Delete(inst.Key);
                            actionManifest.Add(inst.Key, deleteResult);
                            break;

                        default:
                            //SAVE
                            var saveResult = client.Save(inst.Key, inst.Value.Content);
                            actionManifest.Add(inst.Key, saveResult);

                            break;
                    }
                }


                //update local repository
                foreach (var item in actionManifest)
                {
                    //Set isDirty to false for all processing results > 0
                    if (item.Value != (int)ProcessState.Incomplete)
                    {
                        var updatefilter = Builders<UserVO>.Filter.Eq("_id", item.Key);
                        var updateInstruction = Builders<UserVO>.Update
                            .Set("isDirty", false);
                        var updateResult = collection.UpdateOne(updatefilter, updateInstruction);

                        if (updateResult.IsAcknowledged)
                        { 
                            //Mark as completed
                            serializedInstructionSet.Where(s => s.Key == item.Key).SingleOrDefault().Value.SetComplete();
                        }
                    }
                }


                return serializedInstructionSet.Values.ToList();

            }

        }

        #region Embedded class to store processing results -- TODO: REFACTOR

        protected enum ProcessState
        {
            Incomplete = 0,
            Complete = 1
        }

        public enum ActionName
        {
            Save,
            Delete
        }

        public class Instruction
        {
            public Instruction()
            {
                this.ActionCompleted = false;
            }
            public string Identifier { get; set; }
            public ActionName ActionName { get; set; }
            public string Content { get; set; }
            public bool ActionCompleted { get; private set; }

            /// <summary>
            /// Sets the complete state.
            /// </summary>
            public void SetComplete()
            {
                this.ActionCompleted = true;
            }

            /// <summary>
            /// Returns a <see cref="System.String" /> that represents this instance.
            /// </summary>
            /// <returns>
            /// A <see cref="System.String" /> that represents this instance.
            /// </returns>
            public override string ToString()
            {
                return String.Format("{{'id':'{0}', 'Action':'{1}', 'Completed':{2}}}",  this.Identifier, this.ActionName.ToString(), this.ActionCompleted);
            }
        }

        #endregion
    }
}
