﻿using ContentMonitor.Contracts.Assets.Audit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContentMonitor.Contracts.Assets.Content
{
    public abstract class AbstractAuditableContentTypeVO  
    {
        public AbstractAuditableContentTypeVO()
        { 
        }
   

        public string Name { get; set; }

    }
}
