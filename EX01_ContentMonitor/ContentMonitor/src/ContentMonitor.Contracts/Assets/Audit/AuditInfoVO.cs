﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContentMonitor.Contracts.Assets.Audit
{
    public class AuditInfoVO
    {
        public AuditInfoVO()
        {

        }
        public string userName { get; set; }
        public string operationName { get; set; }
        public DateTime operationTime { get; set; }
    }
}
