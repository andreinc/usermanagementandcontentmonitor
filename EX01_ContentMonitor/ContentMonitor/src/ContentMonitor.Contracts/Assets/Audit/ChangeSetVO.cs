﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContentMonitor.Contracts.Assets.Audit
{ 
    public class ChangeSetVO
    {
        public string Author { get; set; }

        public ChangeSetVO()
        {
            this.Changes = new List<ChangeVO>();
            this.ContentTypeVerb = "updated";
        }

        public string ContentTypeEvaluated { get; set; }
        public string ContentTypeVerb { get; set; }

        public string ContentIdentifyingName { get; set; }

        /// <summary>
        /// Gets or sets the changes.
        /// </summary>
        /// <value>
        /// The changes.
        /// </value>
        private IList<ChangeVO> Changes { get; set; }

        public List<ChangeVO> GetDetectedChanges() {
            return this.Changes.ToList();
        }

        /// <summary>
        /// Appends the change.
        /// </summary>
        /// <param name="change">The change.</param>
        public void AppendChange(ChangeVO change)
        {
            this.Changes.Add(change);
        }

        /// <summary>
        /// Appends the change.
        /// </summary>
        /// <param name="propertyname">The propertyname.</param>
        /// <param name="previousValue">The previous value.</param>
        /// <param name="updatedValue">The updated value.</param>
        /// <param name="typeName">Name of the type.</param>
        public void AppendChange(string propertyName, string previousValue, string updatedValue, string typeName)
        {
            this.Changes.Add(new ChangeVO(propertyName, previousValue, updatedValue, typeName));
        }

        public string SummaryChangeToString()
        {
            //“Kevin Jones updated user X”, 
            return string.Format("{0} {1} {2} {3}", this.Author, this.ContentTypeVerb, this.ContentTypeEvaluated, this.ContentIdentifyingName);
        }

        public List<string> DetailedChangesToString()
        {
            var col = new List<string>();

            foreach (ChangeVO change in Changes)
            {
                col.Add(string.Format("field {0}, original value: {1}, new value: {2}",  change.PropertyName, change.PreviousValue , change.UpdatedValue));
            }

            return col;
        }

    }
}
