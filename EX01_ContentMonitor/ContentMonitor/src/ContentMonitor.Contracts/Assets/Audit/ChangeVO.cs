﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContentMonitor.Contracts.Assets.Audit
{
    public class ChangeVO
    {
        public string PreviousValue { get; set; }
        public string PropertyName { get; set; }
        public string TypeName { get; set; }
        public string UpdatedValue { get; set; }
        public DateTime Updated { get; set; }


        /// <summary>
        /// Initializes a new instance of the <see cref="ChangeVO"/> class.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="previousValue">The previous value.</param>
        /// <param name="updatedValue">The updated value.</param>
        /// <param name="typeName">Name of the type.</param>
        public ChangeVO(string propertyName, string previousValue, string updatedValue, string typeName)
        {
            this.PropertyName = propertyName;
            this.PreviousValue = previousValue;
            this.UpdatedValue = updatedValue;
            this.TypeName = typeName;
            this.Updated = DateTime.Now;
        }
    }
}
