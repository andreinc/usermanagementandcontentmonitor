﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContentMonitor.Contracts.Assets.Content
{
    public class PigeonVO : AnimalVO
    {
        public PigeonVO()
        {
            base.Kingdom = "Animalia";
            base.Phylum = "Chordata";
            base.Class = "Aves";
            base.Clade = "Columbimorphae";
            base.Order = "Columbiformes";
            base.Family = "Columbidae";
        }

        public int YearsInNewYork { get; set; }
        public string Bearing { get; set; }
        public int FlockName { get; set; }
    }
}
