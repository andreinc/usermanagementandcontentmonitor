﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContentMonitor.Contracts.Assets.Content
{
    public class DogVO : AnimalVO
    {

        public DogVO()
        {
            base.Kingdom = "Animalia";
            base.Phylum = "Chordata";
            base.Clade = "Synapsida";
            base.Class = "Mammalia";
            base.Order = "Carnivora";
            base.Suborder = "Caniformia";
            base.Family = "Canidae";
            base.Genus = "Canis";
            base.Species = "C.lupus";
            base.Subspecies = "C.l.familiaris";
        }

        public string FavoriteToy { get; set; }
        public int DogYears { get; set; }
        public string PupName { get; set; }
    }
}
