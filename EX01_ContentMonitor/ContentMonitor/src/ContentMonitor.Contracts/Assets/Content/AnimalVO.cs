﻿using ContentMonitor.Contracts.Assets.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContentMonitor.Contracts.Assets.Content
{
    public class AnimalVO : AbstractAuditableContentTypeVO
    {


        public string Kingdom 	 { get; set; }
        public string Phylum 	 { get; set; }
        public string Clade 	 { get; set; }
        public string Class 	 { get; set; }
        public string Order 	 { get; set; }
        public string Suborder 	 { get; set; }
        public string Family 	 { get; set; }
        public string Genus 	 { get; set; }
        public string Species 	 { get; set; }
        public string Subspecies { get; set; }

         
    }
}
