﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContentMonitor.Contracts.Assets.Content
{
    public class BookVO : AbstractAuditableContentTypeVO
    {

        public string ISBN { get; set; }
        public string Category { get; set; }
        public DateTime Published { get; set; }
        public string Title { get; set; }
        public string AuthorFullName { get; set; }
    }
}
