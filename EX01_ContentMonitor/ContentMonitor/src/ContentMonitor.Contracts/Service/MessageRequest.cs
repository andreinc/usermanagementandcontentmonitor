﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContentMonitor.Contracts.Service
{
    public class MessageRequest<T> where T : new()
    {
        public MessageRequest()
        {
            this.Criteria = new T();
        }

        public string username { get; set; }
        public string FullName { get; set; }

        public T Criteria { get; set; }
    }
}
