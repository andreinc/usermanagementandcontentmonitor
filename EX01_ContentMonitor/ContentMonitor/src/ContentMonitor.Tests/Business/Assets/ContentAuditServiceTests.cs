﻿using ContentMonitor.Business.Assets;
using ContentMonitor.Contracts.Assets.Audit;
using ContentMonitor.Contracts.Assets.Content;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace ContentMonitor.Tests.Business.Assets
{
    public class ContentAuditServiceTests
    {
        [Fact]
        void TestChangeOnDog()
        {
            var aDog = new DogVO();
            aDog.PupName = "Fido"; 
            aDog.FavoriteToy = "RubberDucky";
            aDog.DogYears = 5;

            var AuditDetails = new AuditInfoVO()
            { 
                 userName = "Kevin Jones",
                 operationTime = DateTime.Now,
                 operationName = "updated"
            };

            //Deep Clone - simulates object being passed as JSON through wire
            string serialized = JsonConvert.SerializeObject(aDog); 
            var bDog = JsonConvert.DeserializeObject<DogVO>(serialized);

            bDog.FavoriteToy = "Left Shoe";
            bDog.PupName = "Fido the second";
            bDog.Class = "Fidous Caninus Wrong Edit";


            bDog.DogYears = 5;      //Set but not changed

            var ctrl = new ContentAuditService();

            var changes  = ctrl.Process(aDog, bDog, AuditDetails);

            //4 changes detected
            Assert.Equal<int>(3, changes.GetDetectedChanges().Count);

            Console.WriteLine(changes.SummaryChangeToString());
            foreach (var item in changes.DetailedChangesToString())
            {
                Console.WriteLine(item);
            }
        }

        [Fact]
        void TestChangeOnUser()
        {


            var AuditDetails = new AuditInfoVO()
            {
                userName = "Kevin Jones",
                operationTime = DateTime.Now,
                operationName = "updated"
            };

            var userX = new PersonVO();
            userX.Name = "X";
            userX.DateOfBirth = new DateTime(1988, 1, 29); 
            userX.FirstName = "Adam";
            userX.LastName = "Lancaster";
            userX.Address = "123 Orchard Ave.";

            //Deep Clone - simulates object being passed as JSON through wire
            string serialized = JsonConvert.SerializeObject(userX);
            var userXmodified = JsonConvert.DeserializeObject<PersonVO>(serialized);

             




            userXmodified.Address = "123 Cedar Road.";

            var ctrl = new ContentAuditService();

            var changes = ctrl.Process(userX, userXmodified, AuditDetails);


            Assert.Equal<int>(1, changes.GetDetectedChanges().Count);
            var printedInfo = changes.SummaryChangeToString();


            Assert.Equal<string>("Kevin Jones updated PersonVO X", printedInfo);

            var soleChange = changes.GetDetectedChanges()[0];
            var expected = String.Format("field {0}, original value: {1}, new value: {2}",
                "Address",
                "123 Orchard Ave.",
                "123 Cedar Road.");
            var actual = changes.DetailedChangesToString().FirstOrDefault();
            Assert.Equal<string>(expected, actual);

            

        }
    }
}
