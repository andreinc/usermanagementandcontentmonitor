﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContentMonitor.Business.Assets
{
    public interface ILogger
    {
        void LogEntry(string type, string message);
    }
}
