﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ContentMonitor.Business.Assets
{
    /// <summary>
    /// 
    /// Using a Mock Object as unsure if entlib is available fully for dotnet.core1.5
    /// 
    /// see actual unit tests for tested log outputs
    /// 
    /// </summary>
    /// <seealso cref="ContentMonitor.Business.Assets.ILogger" />
    public class MockLogger : ILogger
    {
        public void LogEntry(string type, string message)
        {
            // Add a facility to the assembly to handle the behaviour to log or store the content
        }
    }
}
