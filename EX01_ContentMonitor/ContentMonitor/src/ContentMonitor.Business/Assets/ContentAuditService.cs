﻿using ContentMonitor.Contracts.Assets.Audit;
using ContentMonitor.Contracts.Assets.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ContentMonitor.Business.Assets
{
    public sealed class ContentAuditService
    {
        private ILogger logFacility;

        #region Constructor

        public ContentAuditService(ILogger logFacility)
        {
            this.knownTypes = new Dictionary<Type, List<PropertyInfo>>();
            this.logFacility = logFacility;
        }

        public ContentAuditService() : this(new MockLogger())
        { 
        }

        #endregion

        #region Cached Reflected Types
        private Dictionary<Type, List<PropertyInfo>> knownTypes;
        /// <summary>
        /// Gets the type information.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        private List<PropertyInfo> GetTypeInfo(Type type)
        {
            //The dictionnary will store the reflected type property set as it is an expensive operation
            var info = new List<PropertyInfo>();

            if (!this.knownTypes.ContainsKey(type))
            { 
                this.knownTypes.Add(type, type.GetRuntimeProperties().ToList());
            }

            info = this.knownTypes[type];

            return info;
        }
        #endregion


        /// <summary>
        /// Processes detects changes netween the specified original and updated objects.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="original">The original.</param>
        /// <param name="updated">The updated.</param>
        /// <param name="changeInfo">The change information.</param>
        /// <returns></returns>
        public ChangeSetVO Process<T>(T original, T updated, AuditInfoVO changeInfo) where T : AbstractAuditableContentTypeVO
        {
            //the constraint limits the range of objects available for comparison

            var detectedChanges = new ChangeSetVO();

            if (original != null && updated != null)
            {
                detectedChanges.ContentTypeEvaluated = typeof(T).Name;
                detectedChanges.Author = changeInfo.userName ?? string.Empty;
                detectedChanges.ContentIdentifyingName = original.Name;
                detectedChanges.ContentTypeVerb = changeInfo.operationName;

                var profile = this.GetTypeInfo(typeof(T));

                //each known property is checked for both instances
                foreach (var prop in profile)
                {
                    try {
                        //very basic comparison based on expected set value and implementation of a ToString()
                        var stringOriginal = prop.GetValue(original) == null ? string.Empty : prop.GetValue(original).ToString();
                        var stringUpdated = prop.GetValue(updated) == null ? string.Empty : prop.GetValue(updated).ToString();
                        if (!stringOriginal.Equals(stringUpdated))
                        {
                            detectedChanges.AppendChange(prop.Name, stringOriginal, stringUpdated, prop.PropertyType.Name);
                        }

                    }
                    catch (Exception e)
                    { 
                        //An unexpected value fetch runtime error
                        this.logFacility.LogEntry("error", e.ToString());
                    }
                    
                }

            }

            #region Log results of processing
            //Log changes detected from both objects
            this.logFacility.LogEntry("info", detectedChanges.SummaryChangeToString());

            foreach (var item in detectedChanges.DetailedChangesToString())
            { 
                this.logFacility.LogEntry("info", item);
            }
            #endregion



            return detectedChanges;
        }
    }
}
