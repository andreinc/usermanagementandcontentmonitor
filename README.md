#Instructions/Solutions

* install mongoDB 3.2: ensure db is reachable at localhost: [ mongodb://localhost:27017 ]
* install nodejs/npm with support for angular 2
* .NET IDE : Visual Studio 2015 / C#

**EX01_ContentMonitor\ContentMonitor: .NET Core 1.5**

* entry point to observe behavior is the Test project [ContentMonitor.Tests]
* From Visual studio Run all Tests (debug as needed to observe expected change detection)
* no front end due to time consideration

**EX02_UserManagement\UserManagementWebApplication : nodejs/angular2**

* at root run install for dependencies: 
```
npm install
```

* to launch the web application at root run: 
```
npm start
```

**EX02_UserManagement\UserManagementProcessor : C# Windows Console**

* Launch via Visual Studio 2015
* entry project is : UserManagementProcessor.ConsoleApp
* mongo db settings are accessible via app.config (pull up from UserManagementProcessor.Business)

# Post implementation notes

* Exercise 1 only loosely meets the requirement for 'Arbitrary' object. The implementation is still based on designed and expected class definitions.
* I would explore reflecting from anonymous class instances, or objects inflated from a serialized state. This would also expose cases with missing property sets or mismatching property types

# Discussion points / Notes
** What do the endpoints look like? How would you handle versioning?**

* For the versioning the responsibility is given to the CommonAdapterUtils extension class in order to take into account the local storage message version and the target contract from the 3rd party. A strategy based on XSLT/XSD transform was considered - with a version for the input/output a proper XSLT could be picked up from an asset repository for an applied transform.
 
** What kind of tests would you write?**

* Validation of content completeness following a transform
* automation of XSD validation for test transforms
* edge cases and supported cases for message conversion 
 
** Soft or hard delete?**

* soft delete for anything on the front end processing prior to the sync-up
* once a local record is deleted and sync'ed to the backend it could safely be deleted
* another option is archiving or distinct file set for content deleted and successfully sync'ed
 
** How to deal with network or external system failures? (e.g. API call w/ retries? bus? queue?)**

* failures to sync on data should not affect the data state on the local side (non third party), with a logging/notification mechanism in place notice of failures should be raised, otherwise since the sync is one way I would fo simplicity rely on a retry. Note that the web application is not responsible in this design for the interaction with third party (this is a queue implementation using the front database as opposed to a dedicated message queing system) 
 
** Why did you pick the libraries you picked?**

* Newtonsoft.Json object serializiation
* MongoDB.Driver for C#/MongoDB API
* Mongoose : package for node.js and MongoDB
* jsonwebtoken: package for identity claims in nodejs and securing access to data modifying API
* password-hash : package for password hash




#Problem Provided

# Straightforward Coding Exercise:
Write a class that helps create an audit trail for arbitrary objects. The primary method's signature might accept an 'old' object and a 'changed' object.  An example of information we would want to be able to retrieve is an update summary, e.g. 'Kevin Jones updated user X', and a list of specific updates, e.g. 'field xyz, original value: green, new value: red'.

# Code Organization / Design Exercise:
Spend about 2 hours coding an API-driven solution to add, update, and retrieve users.

#Requirements
* Need to be able to add a user, save a user in 'patches' (e.g. user basic info, user contact info), delete a user
* Need to be able to query users (e.g. an endpoint that drives a list view that returns a few of the user fields) and view a page with detailed info about a user
* Need to be able to sync all changes made to a user to an external system. Assume the external system provides you with an SDK. To use the SDK, you create a new instance of a class ExternalUserClient with a 'Save' and a 'Delete' method (you can create a dummy class by this name that does nothing). Note that the User object of ExternalUserClient might differ slightly from the one in the domain of your project.
* Need to validate and authorize requests 

# General Instructions
* Prefer C#, but feel free to use a different language
* Code should compile, but does not need to function
* Focus on the back-end only, i.e. API endpoint down to the data storage layer
* Use any libraries or frameworks you'd like
* Stub out implementation details for things like validation, data access, etc
* Comment generously on design and implementation details

# Discussion points
* What do the endpoints look like? How would you handle versioning?
* What kind of tests would you write?
* Soft or hard delete?
* How to deal with network or external system failures? (e.g. API call w/ retries? bus? queue?)
* Why did you pick the libraries you picked?